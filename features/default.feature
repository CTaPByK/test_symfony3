Feature: Default
  In order to test default symfony page
  As a website user
  I need to be able to see welcome page

  @javascript
  Scenario: Search for a welcome page text
    Given I am on "/"
    Then I should see "Welcome to Symfony 3.3.10"
    And I wait 5 seconds
